/*
  Copyright (C) 2017 Andre Plante
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include "T_Thermo.h"

void setup() {
  
  Serial.begin(9600);
  
}

void loop(){

  // Compute dew point depression for a relative humidity of 70%
  // at 20 C and normal pressure 10130 Pa.
  // T_tcdk is 0 C in Kelvin and serves to convert from C to K
  // Relative humidity is 70% (.7)
  // Temperature is 20. C
  // Pressure is 10130. Pa
  // Result should be 5.64 degre C
  float res = T_shraes(.7,20.+T_tcdk,10130.,1);
  if( fabs(res - 5.64) > 0.01 ){
    Serial.print("Something is wrong, expecting 5.64 got");
    Serial.println(res);
  } else{
    Serial.println("It is working fine");
  }
  delay(1000);
  
}
