import os
from ctypes import *
import ctypes

class thermo:
    def __init__(self):
        self.tcdk = 273.15
        try:
            self.thermo = CDLL(os.environ['PWD']+'/T_Thermo.so')
        except:
            try:
                self.thermo = CDLL(os.environ['T_THERMO']+'/T_Thermo.so')
            except:
                raise OSError("Could not load T_Thermo dynamic library")
        self.thermo.T_shraes.argtypes = [ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_int]
        self.thermo.T_shraes.restype = ctypes.c_float
    
    def shraes(self, hr, tt, ps, swph):
        return self.thermo.T_shraes(hr, tt, ps, swph)

        
        
