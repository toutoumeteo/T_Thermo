import os
import sys
import numpy as np
import math
from t_thermo import thermo

th = thermo()

#test th.tcdk

file_name="../data/shraes.txt"

try:
    fo  = open(file_name, "r")
except:
    raise OSError("Could open file "+file_name)


for tt in np.arange(-19.5, 40.51, 1.):
    for hr in np.arange(.1, 1.01, .1):
        for ps in np.arange(990., 1020.1, 10.):
            res = th.shraes(hr, tt+th.tcdk, ps*100., 1)
            ctrl = fo.readline().split()
            #print(ctrl[0] + ' ,' + ctrl[1] + ' ,' + ctrl[2] + ' ,' + ctrl[3] )            
            #print(str(hr) + ' ,' + str(tt) + ' ,' + str(ps) + ' ,' + str(res) )
            if(math.fabs( float(ctrl[3]) - res ) > 1.e-4):
                print(ctrl[0] + ' ,' + ctrl[1] + ' ,' + ctrl[2] + ' ,' + ctrl[3] )            
                print(str(hr) + ' ,' + str(tt) + ' ,' + str(ps) + ' ,' + str(res) )
                raise OSError("OUPS")
print("Test passed")

