!  Copyright (C) 2017 Andre Plante
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
! Based on Environment Canada thermodynamic coanstant and function

program use_shrahu

  implicit none
  
  integer :: i, j, k
  real :: tt, hr, ps, hu, shrahu3, res
  logical :: ok, write_control_L = .false.

include "thermoconsts.inc"
include "dintern.inc"
include "fintern.inc"

  ok = .true.

  if(write_control_L)then
     open(unit=10,file='../data/shrahu.txt',status='UNKNOWN')
     do i=-20,40
        do j=1,10
           do k=990,1020,10
              tt = i + .5
              hr = j / 10.
              ps = k
              res= shrahu3(hr,tt+tcdk,ps*100.,.true.)
              write(10,*)hr, tt, ps, res
           enddo
        enddo
     enddo
  else
     open(unit=10,file='../data/shrahu.txt',status='OLD')
     do i=-20,40
        do j=1,10
           do k=990,1020,10
              tt = i + .5
              hr = j / 10.
              ps = k              
              read(10,*)hr, tt, ps, hu
              if( abs(hu - shrahu3(hr,tt+tcdk,ps*100.,.true.))/hu  > epsilon(hu) )then
                 print*,'OUPS',hr, tt, ps, hu, shrahu3(hr,tt+tcdk,ps*100.,.true.)
                 ok = .false.
              endif
           enddo
        enddo
     enddo
     if(ok)then
        print*,'Test passed'
     else
        print*,'Test failed'
     endif
  endif
  close(10)
  
end program use_shrahu
