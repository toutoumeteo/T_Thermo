real(kind=8) function my_foewf(ttt) result(val)

  implicit none
  
  real :: ttt
  include "thermoconsts.inc"  

  val = min( sign(ttns3w, dble(ttt)-dble(trpl)), sign(ttns3i, dble(ttt)-dble(trpl)) ) &
       *abs(dble(ttt)-dble(trpl)) &
       /( dble(ttt)-ttns4w + max(0.d0, sign(ttns4w-ttns4i,dble(trpl)-dble(ttt))) )
  
end function my_foewf
