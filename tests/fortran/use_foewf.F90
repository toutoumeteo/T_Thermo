!  Copyright (C) 2017 Andre Plante
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
! Based on Environment Canada thermodynamic coanstant and function

program use_foewf

  implicit none

  integer :: i, j
  real :: tt
  real*8 :: es
  logical :: ok, write_control_L = .false.

include "thermoconsts.inc"
include "dintern.inc"
include "fintern.inc"

  ok = .true.

  if(write_control_L)then
     open(unit=10,file='../data/foewf.txt',status='UNKNOWN')
     do i=-20,40
        do j=0,9
           tt = i + .1*j
           write(10,*)tt, foewf(tt+tcdk)
        enddo
     enddo
  else
     open(unit=10,file='../data/foewf.txt',status='OLD')
     do i=-20,40
        do j=0,9
           tt = i + .1*j
           read(10,*) tt, es
           if( abs(es - foewf(tt+tcdk)) > epsilon(es) )then
              print*,'OUPS', tt, es
              ok = .false.
           endif
        enddo
     enddo
     if(ok)then
        print*,'Test passed'
     else
        print*,'Test failed'
     endif
  endif
  close(10)
  
end program use_foewf
