/*
  Copyright (C) 2017 Andre Plante
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Based on Environment Canada thermodynamic coanstant and function

#include <stdio.h>
#include <math.h>

#include "T_Thermo.h"

int main( int argc, char *argv[]){

  FILE *fp;
  int i, j, ok;
  float tt;
  double es;

  fp=fopen("../data/foew.txt", "r");
  
  ok = 1;
  for( i=-20; i<=40; i++){
    for( j=0; j<=9; j++){
      fscanf(fp, "%f %lf", &tt, &es);
      if( (fabs(es - T_foew(tt+T_tcdk)))/es > .00001){
	ok = 0;
	printf("%f %lf %lf\n", tt, es, T_foew(tt+T_tcdk));
      }
    }
  }  
  fclose(fp);
  if(ok){
    printf("Test passed\n");
    return 0;
  }else{
    printf("Test failed\n");
    return 1;
  }

} 
