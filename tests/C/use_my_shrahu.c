/*
  Copyright (C) 2017 Andre Plante
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Based on Environment Canada thermodynamic coanstant and function

#include <stdio.h>
#include <math.h>

#include "T_Thermo.h"

int main( int argc, char *argv[]){

  FILE *fp;
  int i, j, k, ok;
  float tt, hr, ps, hu, res;

  fp=fopen("../data/shrahu.txt", "r");
  
  ok = 1;
  for( i=-20; i<=40; i++){
    for( j=1; j<=10; j++){      
      for( k=990; k<=1020; k+=10){
	fscanf(fp, "%f %f %f %f", &hr, &tt, &ps, &res);
	if( fabs( res-T_shrahu(hr,tt+T_tcdk,ps*100.,1) ) / res > .00001){
	  ok = 0;
	  printf("%f %f %f %f %f\n", hr, tt, ps, res, T_shrahu(hr,tt+T_tcdk,ps*100.,1));
	}
      }
    }
  }  
  fclose(fp);
  if(ok){
    printf("Test passed\n");
    return 0;
  }else{
    printf("Test failed\n");
    return 1;
  }

} 
