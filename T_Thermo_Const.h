/*
  Copyright (C) 2017 Andre Plante
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Based on Environment Canada thermodynamic coanstants

#ifndef __T_THERMO_CONST__
#define __T_THERMO_CONST__

#define T_water_switch 1
#define T_ice_switch 0
#define T_ttns3w 17.269
#define T_trpl 0.2731600000000e+03
#define T_ttns3i 21.875
#define T_ttns4w 35.86
#define T_ttns4i 7.66
#define T_tcdk 0.2731500000000e+03
#define T_ttns1 610.78
#define T_eps1 0.6219800221014
#define T_eps2 0.3780199778986
#endif
