/*
  Copyright (C) 2017 Andre Plante
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Based on Environment Canada thermodynamic functions

#ifndef __T_THERMO_FUNC__
#define __T_THERMO_FUNC__

#define T_sign(x,y)  ((y >= 0) ? fabs(x) : (-fabs(x)))
#define T_min(x,y)  (((x) < (y)) ? (x) : (y))
#define T_max(x,y)  (((x) > (y)) ? (x) : (y))

// All units are mKs, temperature in K, pressure in Pa
// Relative humidity is in %, e.g. 0.7 for 70%
// Absolute humidity in kg per kg of dry air

// Saturation water vapor pressure (from Tetens) relative to water or ice depending on temperature (ttt)
#define T_foewf(ttt) ( T_min( T_sign(T_ttns3w, (ttt)-T_trpl), T_sign(T_ttns3i, (ttt)-T_trpl) ) * fabs((ttt)-T_trpl) / ( (ttt)-T_ttns4w + T_max(0.0, T_sign(T_ttns4w-T_ttns4i,T_trpl-(ttt)) ) ) )
#define T_foew(ttt) ( T_ttns1 * exp(T_foewf(ttt)) )

// Compute specific humidity (qqq) from water vapor pression (eee) and total atmospheric pressure (prs)
#define T_foqfe(eee,prs) ( T_min(1.0,T_eps1*(eee) / ( (prs) - T_eps2*(eee) )) )

// Compute vapor pressure (eee) from specific humidity (qqq) and atmopheric pressure (prs)
#define T_foefq(qqq,prs) ( T_min( (prs), ((qqq)*(prs)) / ( T_eps1 + T_eps2*(qqq))) )

float T_shrahu(float hr, float tt, float ps, int swph){
  // Compute specific humidity from relative humidity (hr),
  // temperature (tt) and atmospheric pressure (ps).
  // If swph == 1 computation over ice, swph over water (not implemented yet)
  float ee;
  
  if(swph){    
    ee = T_min(ps, hr * T_foew(tt) );
  } else {
    printf("To do in T_shrahu\n");
    return -1;
    //ee = T_min(ps, hr * foewa(tt) );
  }
  return T_foqfe(ee, ps);
}

float T_shuaes(float hu, float tt, float ps, int swph){
  // Compute dew point depression from specific humidity (hr),
  // temperature (tt) and atmospheric pressure (ps).
  // If swph == 1 computation over ice, swph over water (not implemented yet)  
  float petit = 0.000001;
  float ee, cte, td;
  ee = T_foefq(T_max(petit,hu),ps);
  cte = log(ee/T_ttns1);
  td = (T_ttns4w*cte - T_ttns3w*T_trpl) / (cte - T_ttns3w);
  if( td < T_trpl && swph ){
    td = ( T_ttns4i*cte - T_ttns3i*T_trpl)/(cte - T_ttns3i);
  }
  return tt-td;
}

float T_shraes(float hr, float tt, float ps, int swph){
  // Compute dew point depression from relative humidity (hr),
  // temperature (tt) and atmospheric pressure (ps).
  // If swph == 1 computation over ice, swph over water (not implemented yet)
  float hus;
  hus = T_shrahu(hr,tt,ps,swph);
  return T_shuaes(hus,tt,ps,swph);

}

#endif
